package PeselAPI;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class PeselInvalidMonthAndYearTest extends PeselBaseTest {

    @Test
    public void peselInvalidMonthFemaleTest ( ) {

        given ( )
                .queryParam ( "pesel", "17140196920" )
        .when ( )
                .get ( )
        .then ( )
                .body ( "errors.errorCode[1]", equalTo ( "INVM" ) )
                .body ( "errors.errorMessage[1]", equalTo ( "Invalid month." ) )
                .body ( "birthDate", equalTo ( null ) )
                .body ( "sex", equalTo ( "Female" ) )
                .assertThat ().statusCode ( 200 );
    }

    @Test
    public void peselInvalidYearFemaleTest ( ) {

        given ( )
                .queryParam ( "pesel", "17140196920" )
        .when ( )
                .get ( )
        .then ( )
                .body ( "errors.errorCode[0]", equalTo ( "INVY" ) )
                .body ( "errors.errorMessage[0]", equalTo ( "Invalid year." ) )
                .body ( "birthDate", equalTo ( null ) )
                .body ( "sex", equalTo ( "Female" ) )
                .assertThat ().statusCode ( 200 );
    }

    @Test
    public void peselInvalidMonthMaleTest ( ) {

        given ( )
                .queryParam ( "pesel", "17140196913" )
        .when ( )
                .get ( )
        .then ( )
                .body ( "errors.errorCode[1]", equalTo ( "INVM" ) )
                .body ( "errors.errorMessage[1]", equalTo ( "Invalid month." ) )
                .body ( "birthDate", equalTo ( null ) )
                .body ( "sex", equalTo ( "Male" ) )
                .assertThat ().statusCode ( 200 );
    }

    @Test
    public void peselInvalidYearMaleTest ( ) {

        given ( )
                .queryParam ( "pesel", "17140196913" )
        .when ( )
                .get ( )
        .then ( )
                .body ( "errors.errorCode[0]", equalTo ( "INVY" ) )
                .body ( "errors.errorMessage[0]", equalTo ( "Invalid year." ) )
                .body ( "birthDate", equalTo ( null ) )
                .body ( "sex", equalTo ( "Male" ) )
                .assertThat ().statusCode ( 200 );
    }
}
