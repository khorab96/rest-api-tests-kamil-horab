package PeselAPI;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;


public class PeselInvalidCharactersTest extends PeselBaseTest {

    @Test
    public void peselInvalidCharactersTest ( ) {

        given ( )
                .queryParam ( "pesel", "791klk134896" )
        .when ( )
                .get ( )
        .then ( )
                .body ( "errors.errorCode[0]", equalTo ( "NBRQ" ) )
                .body ( "errors.errorMessage[0]", equalTo ( "Invalid characters. Pesel should be a number." ) )
                .body ( "birthDate", equalTo ( null ) )
                .body ( "sex", equalTo ( null ) )
                .assertThat ().statusCode ( 200 );
    }
}