package PeselAPI;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class PeselInvalidLenghtTest extends PeselBaseTest {

    @Test
    public void peselInvalidLenghtTest ( ) {

        given ( )
                .queryParam ( "pesel", "117293396929" )
        .when ( )
                .get ( )
        .then ( )
                .body ( "errors.errorCode[0]", equalTo ( "INVL" ) )
                .body ( "errors.errorMessage[0]", equalTo ( "Invalid length. Pesel should have exactly 11 digits." ) )
                .body ( "birthDate", equalTo ( null ) )
                .body ( "sex", equalTo ( null ) )
                .assertThat ().statusCode ( 200 );
    }
}