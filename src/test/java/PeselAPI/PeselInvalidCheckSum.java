package PeselAPI;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class PeselInvalidCheckSum extends PeselBaseTest {

    @Test
    public void peselInvalidCheckSumFemaleTest ( ) {

        given ( )
                .queryParam ( "pesel", "17293096927" )
        .when ( )
                .get ( )
        .then ( )
                .body ( "errors.errorCode[0]", equalTo ( "INVC" ) )
                .body ( "errors.errorMessage[0]", equalTo ( "Check sum is invalid. Check last digit." ) )
                .body ( "birthDate", equalTo ( "2017-09-30T00:00:00" ) )
                .body ( "sex", equalTo ( "Female" ) )
                .assertThat ( ).statusCode ( 200 );
        ;
    }

    @Test
    public void peselInvalidCheckSumMaleTest ( ) {

        given ( )
                .queryParam ( "pesel", "17293096917" )
        .when ( )
                .get ( )
        .then ( )
                .body ( "errors.errorCode[0]", equalTo ( "INVC" ) )
                .body ( "errors.errorMessage[0]", equalTo ( "Check sum is invalid. Check last digit." ) )
                .body ( "birthDate", equalTo ( "2017-09-30T00:00:00" ) )
                .body ( "sex", equalTo ( "Male" ) )
                .assertThat ( ).statusCode ( 200 );
    }
}