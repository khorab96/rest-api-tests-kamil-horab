package PeselAPI;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class ValidPeselTest extends PeselBaseTest {

    @Test
    public void validPeselFemaleTest ( ) {
        given ( ).queryParam ( "pesel", "17293007762" ).
                when ( ).get ( ).
                then ( )
                .body ( "isValid", equalTo ( true ) )
                .body ( "sex", equalTo ( "Female" ) )
                .body ( "birthDate", equalTo ( "2017-09-30T00:00:00" ) )
                .assertThat ().statusCode ( 200 );
    }

    @Test
    public void validPeselMaleTest ( ) {
        given ( ).queryParam ( "pesel", "17293007335" ).
                when ( ).get ( ).
                then ( )
                .body ( "isValid", equalTo ( true ) )
                .body ( "sex", equalTo ( "Male" ) )
                .body ( "birthDate", equalTo ( "2017-09-30T00:00:00" ))
                .assertThat ().statusCode ( 200 );
    }
}
