package PeselAPI;

import io.restassured.RestAssured;
import org.testng.annotations.BeforeClass;

public class PeselBaseTest {

    @BeforeClass
    public static void setup(){
        RestAssured.baseURI = "https://peselvalidatorapitest.azurewebsites.net";
        RestAssured.basePath = "/api/Pesel";
    }
}
