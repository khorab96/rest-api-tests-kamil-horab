package PeselAPI;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class PeselInvalidDayTest extends PeselBaseTest {

    @Test
    public void peselInvalidDayFemaleTest ( ) {

        given ( )
                .queryParam ( "pesel", "17293396929" )
        .when ( )
                .get ( )
        .then ( )
                .body ( "errors.errorCode[0]", equalTo ( "INVD" ) )
                .body ( "errors.errorMessage[0]", equalTo ( "Invalid day." ) )
                .body ( "birthDate", equalTo ( null ) )
                .body ( "sex", equalTo ( "Female" ) )
                .assertThat ().statusCode ( 200 );;
    }

    @Test
    public void peselInvalidDayMaleTest ( ) {

        given ( )
                .queryParam ( "pesel", "17293396912" )
        .when ( )
                .get ( )
        .then ( )
                .body ( "errors.errorCode[0]", equalTo ( "INVD" ) )
                .body ( "errors.errorMessage[0]", equalTo ( "Invalid day." ) )
                .body ( "birthDate", equalTo ( null ) )
                .body ( "sex", equalTo ( "Male" ) )
                .assertThat ().statusCode ( 200 );
    }
}