package PetstoreAPI;

import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;

public class SoldPetsTest extends PendingPetsTest {

    @Test
    public void CheckIfFishPetWasSoldTest ( ) {

        String responseBody = get ( "?status=sold" ).asString ( );
        Assert.assertTrue ( responseBody.contains ( "fish" ), "Fish Pet was not sold yet" );
    }
}
