package PetstoreAPI;

import io.restassured.RestAssured;
import org.testng.annotations.BeforeClass;

public class PetstoreBaseTest {
    @BeforeClass
    public void setup ( ) {
        RestAssured.baseURI = "https://petstore.swagger.io";
        RestAssured.basePath = "/v2/pet/findByStatus";

    }
}
