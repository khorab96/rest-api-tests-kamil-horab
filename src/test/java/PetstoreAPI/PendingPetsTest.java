package PetstoreAPI;

import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;

public class PendingPetsTest extends PetstoreBaseTest {

    @Test
    public void CheckIfFishPetIsNotPendingTest ( ) {

        String responseBody = get ( "?status=pending" ).asString ( );
        Assert.assertTrue ( !responseBody.contains ( "fish" ), "Fish Pet is not pending" );
    }
}
