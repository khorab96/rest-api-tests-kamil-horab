package PetstoreAPI;

import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;

public class AvailablePetsTest extends PetstoreBaseTest {

    @Test
    public void CheckIfFishPetIsAvailableTest ( ) {

        String responseBody = get ( "?status=available" ).asString ( );
        Assert.assertTrue ( responseBody.contains ( "fish" ), "Fish Pet is not available" );
    }
}
